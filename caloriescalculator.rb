class Meal
  attr_reader :food, :calories
  
  def initialize(name, calories=0)
    @name = name
    @calories = calories
  end
end